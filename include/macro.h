#ifndef VB_MATH_MACRO_H
#define VB_MATH_MACRO_H


#undef		vb_abs
#undef		vb_sign
#undef		vb_max
#undef		vb_min
#undef		vb_avg
#undef		vb_max3
#undef		vb_min3
#undef		vb_avg3
#undef		vb_square
#undef		vb_cube
#undef		vb_cube_root
#undef		vb_pow10
#undef		vb_powe
#undef		vb_pow2
#undef		vb_log2
#undef		vb_sum_gauss


#define 	vb_abs(v)			((v) * (((v) > 0) - ((v) < 0)))
#define 	vb_sign(v)			(((v) > 0) ? 1 : (((v) < 0) ? -1 : 0))
#define 	vb_max(a, b)		(((a) > (b)) ? (a) : (b))
#define 	vb_min(a, b)		(((a) < (b)) ? (a) : (b))
#define 	vb_avg(a, b)		(((a) + (b)) / 2)
#define 	vb_max3(a, b, c)	vb_max(a, vb_max(b, c))
#define 	vb_min3(a, b, c)	vb_min(a, vb_min(b, c))
#define 	vb_avg3(a, b, c)	(((a) + (b) + (c)) / 3)
#define 	vb_square(x)		((x) * (x))
#define 	vb_cube(x)			(vb_square((x)) * (x))
#define 	vb_cube_root(x)		vb_pow(x, 1/3)
#define		vb_pow10(n)			vb_pow(10, n)
#define		vb_powe(n)			exp(n)
#define		vb_pow2(n)			(2 << n)
#define		vb_log2(n)			(log(n) / log(2))

#define 	vb_sum_gauss(n)		((n) * ((n) + 1) / 2)

#endif /* VB_MATH_MACRO_H */
