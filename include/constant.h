#ifndef VB_MATH_CONSTANT_H
#define VB_MATH_CONSTANT_H

#undef 		VB_E
#undef 		VB_PI
#undef 		VB_1_E
#undef 		VB_RADE
#undef 		VB_LOG2E
#undef 		VB_LOG2_10
#undef 		VB_LOG10E
#undef 		VB_LN2
#undef 		VB_LN10
#undef 		VB_2PI
#undef 		VB_3PI_4
#undef 		VB_2PI_3
#undef 		VB_PI_2
#undef 		VB_PI_3
#undef 		VB_PI_4
#undef 		VB_PI_5
#undef 		VB_1_PI
#undef 		VB_2_PI
#undef 		VB_RADPI
#undef 		VB_2_RADPI
#undef 		VB_3RAD_2
#undef 		VB_RAD2
#undef 		VB_RAD3
#undef 		VB_PHI
#undef 		VB_FACT_3
#undef 		VB_FACT_4
#undef 		VB_FACT_5
#undef 		VB_FACT_6
#undef 		VB_FACT_7
#undef 		VB_FACT_8
#undef 		VB_FACT_9
#undef 		VB_FACT_10


#define 	VB_E			2.7182818284590452   	// 	e 		
#define 	VB_1_E			0.3678794411714423		//	1/e
#define 	VB_RADE			1.6487212707001282		//	sqrt(e)
#define 	VB_LOG2E		1.4426950408889634   	// 	log2  e 	
#define 	VB_LOG2_10		3.3219280948873626		//	log2 10
#define 	VB_LOG10E		0.4342944819032518  	// 	log10 e 	
#define 	VB_LN2			0.6931471805599453  	// 	loge  2 	
#define 	VB_LN10			2.3025850929940456  	// 	loge 10 	

#define 	VB_PI			3.1415926535897932  	// 	pi
#define 	VB_2PI			6.2831853071795862		//  2*pi
#define 	VB_3PI_4		2.3561944901923449		//	3*pi/4
#define 	VB_2PI_3		2.0943951023931955		//	2*pi/3
#define 	VB_PI_2			1.5707963267948966  	// 	pi/2 	
#define 	VB_PI_3			1.0471975511965976		//	pi/3
#define 	VB_PI_4			0.7853981633974483  	// 	pi/4
#define 	VB_PI_5			0.6283185307179586		//	pi/5
#define 	VB_1_PI			0.3183098861837906  	// 	1/pi 	
#define 	VB_2_PI			0.6366197723675813  	// 	2/pi
#define 	VB_RADPI		1.7724538509055159		//	sqrt(pi)	
#define 	VB_2_RADPI		1.1283791670955125  	// 	2/sqrt(pi) 

#define 	VB_3RAD_2		1.2599210498948732		//	3\|2
#define 	VB_RAD2			1.4142135623730950  	// 	sqrt(2)
#define 	VB_RAD3			1.7320508075688773		//	sqrt(3)
#define 	VB_PHI			1.6180339887498655		//	(1+sqrt(5))/2 phidia

#define		VB_FACT_3		6L
#define		VB_FACT_4		24L
#define		VB_FACT_5		120L
#define		VB_FACT_6		720L
#define		VB_FACT_7		5040L
#define		VB_FACT_8		40320L
#define		VB_FACT_9		362880L
#define		VB_FACT_10		3628800L

#endif /* VB_MATH_CONSTANT_H */
